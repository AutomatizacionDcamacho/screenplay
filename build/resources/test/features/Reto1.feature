#Author: dccamacho@choucairtesting.com
# language:es
@tag
		Característica: Diligencia Pagina Demo
    Quiero ingresar a la página de Demo
    Para diligenciar el formulario

  @CasoExitoso
  Escenario: Diligenciar formulario Automation Demos Site
    Dado que Carlos quiere acceder a la Web Automation Demo Site
    Cuando el realiza el registro en la página
    |Primer Nombre|Apellido|Direccion     |Email2             |Telefono   |Lenguaje   |Skill  |Pais   |Ciudad   |AñoCumple|MesCumple|Dia|Password|ConfirPassword|
    |Diana        |Camacho |Calle35c 87a43|diana.634@gmail.com|4962197989 |Bulgarian  |C      |Algeria|Australia|  1985   |  March     | 26|Diana123|Diana123|
	  Entonces el verifica que se carga la pantalla con texto ("- Double Click on Edit Icon to EDIT the Table Row.")
   

