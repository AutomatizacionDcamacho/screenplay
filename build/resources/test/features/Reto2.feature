#Author: dccamacho@choucairtesting.com
# language:es

@tag
Característica: Ingresar a la pagina OrageHR
  Realizar el registro a la aplicación 
  Para visualizar el empleado creado

  @CasoExitoso
   Escenario: Crear y consultar el empleado en la pagina 
   Dado que Juan necesita crear un empleado en el OrageHR
   Cuando el realiza el ingreso del registro en la aplicación
   |PIM|Primer Nombre|Apellido|Ubicacion|Segundo Nombre|ID         |Cumpleaños|Estado civil| Licencia      |VenLicencia|NicKname|
   |Add Employee|Diana|Camacho|Australian Regional HQ|Carolina|1785|1985/03/26| Single     |85032638738   |2020/03/21|dcamacho|
   Entonces el visualiza el nuevo empleado en el aplicativo


