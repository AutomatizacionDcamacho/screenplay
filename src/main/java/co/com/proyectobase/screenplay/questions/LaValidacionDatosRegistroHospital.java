package co.com.proyectobase.screenplay.questions;


import co.com.proyectobase.screenplay.ui.HospitalHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaValidacionDatosRegistroHospital  implements Question<String>  {

	public static LaValidacionDatosRegistroHospital es() {
		return new LaValidacionDatosRegistroHospital();
	}
	@Override
	public String answeredBy(Actor actor) {
		return Text.of(HospitalHomePage.Texto_label).viewedBy(actor).asString();
	}
}
