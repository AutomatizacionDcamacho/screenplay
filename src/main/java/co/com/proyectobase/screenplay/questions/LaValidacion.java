package co.com.proyectobase.screenplay.questions;

import co.com.proyectobase.screenplay.ui.DemoHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;

public class LaValidacion implements Question<String> {
	
	public static LaValidacion es() {
		return new LaValidacion();
	}
	
		@Override
	public String answeredBy(Actor actor) {
		return Text.of(DemoHomePage.Texto_label).viewedBy(actor).asString();
	}


	}
	


