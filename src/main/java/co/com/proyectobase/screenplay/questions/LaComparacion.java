package co.com.proyectobase.screenplay.questions;


import co.com.proyectobase.screenplay.ui.OrageHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.Text;


public class LaComparacion implements Question<String>{

	public static LaComparacion es() {
			return new LaComparacion();
	}

	@Override
	public String answeredBy(Actor actor) {
		return Text.of(OrageHomePage.LABEL_VERIFICACION).viewedBy(actor).asString();

	}

}
