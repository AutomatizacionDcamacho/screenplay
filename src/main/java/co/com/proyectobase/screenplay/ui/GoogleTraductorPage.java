package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;


public class GoogleTraductorPage extends PageObject{

	
	public static final Target BOTON_LENGUAJE_ORIGEN= Target.the("Botón del idioma origen")
			 .located(By.id("gt-sl-gms"));
	public static final Target BOTON_LENGUAJE_DESTINO= Target.the("Botón del idioma Destino")
			 .located(By.id("gt-tl-gms"));
	public static final Target OPCION_INGLES= Target.the("Opción Ingles")
			 .located(By.xpath("//div[@id='gt-sl-gms-menu']/table/tbody//tr/td//div[contains(text(),'ingl')]"));
	public static final Target OPCION_ESPAÑOL= Target.the("Opción Español")
			 .located(By.xpath("//div[@id='gt-tl-gms-menu']/table/tbody//tr/td//div[contains(text(),'espa')]"));
	public static final Target AREA_TRADUCCION= Target.the("Lugar donde se escribe la palabra a traducir")
			 .located(By.id("source"));
	public static final Target BOTON_TRADUCIR= Target.the("El botón traducir")
			 .located(By.id("gt-lang-submit"));
	public static final Target AREA_TRADUCIDA= Target.the("Lugar donde se presenta el área traducida")
			 .located(By.id("gt-res-dir-ctr"));
}




