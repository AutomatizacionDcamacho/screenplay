package co.com.proyectobase.screenplay.ui;

import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://orangehrm-demo-6x.orangehrmlive.com/")
public class OrageHomePage extends PageObject {
	
	public static final Target BOTON_LOGIN= Target.the("Botón del Login")
			 .located(By.id("btnLogin"));
	public static final Target BOTON_PMI= Target.the("Botón del PMI")
			 .located(By.id("menu_pim_viewPimModule"));
	public static final Target BOTON_ADD= Target.the("Botón del PMI")
			 .located(By.xpath("//*[@id='menu_pim_viewPimModule']//div[@class='collapsible-body' and contains(@style,'block')]"));
	public static final Target AREA_PRIMERNOMBRE= Target.the("Area primer Nombre")
			 .located(By.xpath("//*[@id=\"firstName\"]"));
	public static final Target AREA_APELLIDO= Target.the("Primer Apellido")
			 .located(By.id("lastName"));
	public static final Target AREA_ID1= Target.the("ID")
			 .located(By.id("employeeId"));
	public static final Target LISTA_UBICACION= Target.the("Lista Ubicacion")
			 .located(By.id("location_inputfileddiv"));
	public static final Target UBICACION= Target.the("Ubicacion")
			 .located(By.id("location_inputfileddiv"));
	public static final Target BOTON_GUARDAR= Target.the("Boton_Guardar")
			 .located(By.id("systemUserSaveBtn"));
	public static final Target AREA_SEGUNDO_NOMBRE= Target.the("Segundo Apellido")
			 .located(By.id("middle_name"));
	public static final Target AREA_ID= Target.the("Segundo Id")
			 .located(By.id("other_id"));
	public static final Target FECHA_CUMPLE= Target.the("Cumple")
			 .located(By.id("date_of_birth"));
	public static final Target ESTADO_CIVIL= Target.the("Estado civil")
			 .located(By.id("marital_status_inputfileddiv"));
	public static final Target LISTA_ESTADO_CIVIL= Target.the("Lista Estado civil")
			 .located(By.xpath("//*[@id=\"marital_status_inputfileddiv\"]"));
	public static final Target GENERO= Target.the("Genero")
			 .located(By.xpath("//*[@id=\"pimPersonalDetailsForm\"]/materializecss-decorator[3]/div/sf-decorator[3]/div/ul/li[2]/label"));
	public static final Target NACIONALIDAD= Target.the("Nacionalidad")
			 .located(By.id("nationality_inputfileddiv"));
	public static final Target LISTA_NACIONALIDAD= Target.the("Lista Nacionalidad")
			 .located(By.xpath("//*[@id=\"nationality_inputfileddiv\"]"));
	public static final Target AREA_LICENCIA= Target.the("Numero licencia")
			 .located(By.id("driver_license"));
	public static final Target AREA_VENCIMIENTO_LICENCIA= Target.the("Vencimiento licencia")
			 .located(By.id("license_expiry_date"));
	public static final Target AREA_NICKNAME= Target.the("Nickname")
			 .located(By.id("nickName"));
	public static final Target AREA_TIPO_SANGRE= Target.the("Tipo de sangre")
			 .located(By.id("1_inputfileddiv"));
	public static final Target LISTA_TIPO_SANGRE= Target.the("Lista Tipo de sangre")
			 .located(By.xpath("//*[@id=\"1_inputfileddiv\"]"));
	public static final Target AREA_HOBBIE= Target.the("Hobbie")
			 .located(By.id("5"));
	public static final Target BOTON_SALVAR= Target.the("Guardar")
			 .located(By.xpath("//*[@id=\"content\"]/div[2]/ui-view/div[2]/div/custom-fields-panel/div/form/materializecss-decorator[3]/div/sf-decorator/div/button"));
	public static final Target OPCION_CONSULTAR= Target.the("Botón de consultaI")
			 .located(By.xpath("//*[@id=\"menu_pim_viewEmployeeList\"]"));
	public static final Target CAMPO_BUSQUEDA= Target.the("Campo Busqueda")
			 .located(By.id("employee_name_quick_filter_employee_list_value"));
	public static final Target BOTON_BUSQUEDA = Target.the("El boton de busqueda")
			.located(By.id("quick_search_icon"));
	public static final Target CAMPO_VERIFICAR = Target.the("Campo Verificación")
			.located(By.id("quick_search_icon"));
	public static final Target LABEL_VERIFICACION = Target.the("Label")
			.located(By.xpath("//*[@id=\'employeeListTable\']/tbody/tr/td[2]"));

}






