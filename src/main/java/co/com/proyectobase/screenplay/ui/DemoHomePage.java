package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://demo.automationtesting.in/Register.html")
public class DemoHomePage extends PageObject {
	
	public static final Target AREA__NOMBRE= Target.the("Campo Nombre")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[1]/input"));
	public static final Target AREA_APELLIDO= Target.the("Campo Apellido")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[1]/div[2]/input"));
	public static final Target AREA_DIRECCION= Target.the("Campo Dirección")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[2]/div/textarea"));
	public static final Target AREA_EMAIL= Target.the("Campo Email")		
			.located(By.xpath("//*[@id=\"eid\"]/input"));
	public static final Target AREA_TELEFONO= Target.the("Campo Telefono")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[4]/div/input"));
	public static final Target AREA_GENERO= Target.the("Campo Genero")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[5]/div/label[2]/input"));
	public static final Target AREA_HOBBIE= Target.the("Campo Hobbie")		
			.located(By.id("checkbox2"));
	public static final Target AREA_LENGUAJE= Target.the("Campo Lenguaje")		
			.located(By.id("msdd"));
	public static final Target LISTA_LENGUAJE= Target.the("Lista Lenguaje")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[7]/div/multi-select/div[2]/ul"));
	public static final Target AREA_SKILL= Target.the("Area Skill")		
			.located(By.id("Skills"));
	public static final Target LISTA_SKILL= Target.the("Lista Skill")		
			.located(By.xpath("//*[@id=\"Skills\"]/option[4]"));
	public static final Target AREA_PAIS= Target.the("Area pais")		
			.located(By.id("countries"));
	public static final Target LISTA_PAIS= Target.the("Lista pais")		
			.located(By.xpath("//*[@id=\"countries\"]/option[5]"));
	public static final Target AREA_CIUDAD= Target.the("Campo ciudad")		
			.located(By.xpath("//*[@class='select2-selection select2-selection--single']"));
	public static final Target LISTA_CIUDAD= Target.the("Lista ciudad")		
			.located(By.className("select2-results"));
	public static final Target AREA_CUMPLE= Target.the("Campo cumple")		
			.located(By.xpath("//*[@id=\"yearbox\"]"));
	public static final Target LISTA_AÑOCUMPLE= Target.the("Lista Añocumple")		
			.located(By.xpath("//*[@id=\"yearbox\"]/option[71]"));
	public static final Target AREA_MES_CUMPLE= Target.the("Campo Mescumple")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select/option[4]"));
	public static final Target LISTA_MesCUMPLE= Target.the("Lista MesAñocumple")		
			.located(By.xpath("//*[@id=\"basicBootstrapForm\"]/div[11]/div[2]/select"));							   
	public static final Target AREA_DIA_CUMPLE= Target.the("Campo Diacumple")		
			.located(By.xpath("//*[@id=\"daybox\"]"));
	public static final Target LISTA_DIACUMPLE= Target.the("Lista DíaAñocumple")
			.located(By.xpath("//*[@id=\"daybox\"]/option[27]"));
	public static final Target AREA_CLAVE= Target.the("Campo Clave")		
			.located(By.xpath("//INPUT[@id='firstpassword']"));
	public static final Target AREA_CONFIRMARCLAVE= Target.the("Campo ConfirmarClave")		
			.located(By.xpath("//*[@id=\"secondpassword\"]"));
	public static final Target BOTON_ENVIAR= Target.the("Boton Enviar")		
			.located(By.id("submitbtn"));
	public static final Target Texto_label= Target.the("label")
			.located(By.xpath("/html/body/section/div[1]/div/div[2]/h4[1]"));
}
