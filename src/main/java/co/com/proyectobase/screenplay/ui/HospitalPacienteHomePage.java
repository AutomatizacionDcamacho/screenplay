package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class HospitalPacienteHomePage extends PageObject {
	
	public static final Target BOTON_AGREGAR_PACIENTE= Target.the("Botón Agregar paciente")
			 .located(By.linkText("Agregar Paciente"));
	public static final Target NOMBRE_PACIENTE= Target.the("Nombre Paciente")
			 .located(By.name("name"));
	public static final Target APELLIDO_PACIENTE= Target.the("Apellido Paciente ")
			 .located(By.name("last_name"));
	public static final Target TELEFONO_PACIENTE= Target.the("Telefono Paciente")
			 			 .located(By.name("telephone"));
	public static final Target DOCUMENTO_PACIENTE= Target.the("TDocumento Paciente")
			 			 .located(By.name("identification_type"));
	public static final Target NUMERO_DOCUMENTO_PACIENTE= Target.the("NumeroDocumento Paciente")
			 .located(By.name("identification"));
	public static final Target SALUD_PREPAGADA= Target.the("Salud Prepagada")
			 .located(By.name("prepaid"));
	public static final Target BOTON_GUARDAR_PACIENTE= Target.the("Salud Prepagada")
			 .located(By.xpath("//A[@onclick='submitForm()'][text()='Guardar']"));
	public static final Target Texto_label= Target.the("Datos guardados correctamente.")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div[2]/p"));
	
	
}
