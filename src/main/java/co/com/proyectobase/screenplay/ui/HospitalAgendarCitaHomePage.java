package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class HospitalAgendarCitaHomePage extends PageObject {
	
	public static final Target BOTON_AGENDAR_CITA= Target.the("Botón Agendar cita")
			 .located(By.xpath("(//I[@class='fa fa-fw fa-tasks'])[6]"));
	public static final Target DIA_CITA= Target.the("Día Cita")
			 .located(By.xpath("//INPUT[@id='datepicker']"));
	public static final Target CLICK= Target.the("Clic")
			 .located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]"));
	public static final Target DOC_PACIENTE= Target.the("Documento Paciente")
			 .located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[2]/input"));
	public static final Target DOC_DOCTOR= Target.the("Documento Doctor")
			 .located(By.xpath("(//INPUT[@type='text'])[3]"));
	public static final Target OBSERVACIONES= Target.the("Observaciones")
			 .located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[3]/div/div[4]/textarea"));
	public static final Target BOTON_GUARDAR_CITA= Target.the("Botón Guardar Cita")
			 .located(By.xpath("//A[@onclick='submitForm()'][text()='Guardar']"));
	

}
