package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl ("http://automatizacion.herokuapp.com/pperez/home")
public class HospitalHomePage extends PageObject{
	
	public static final Target Texto_label= Target.the("Datos guardados correctamente.")
			.located(By.xpath("//*[@id=\"page-wrapper\"]/div/div[2]/div[2]/p"));
}
