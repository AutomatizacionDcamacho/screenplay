package co.com.proyectobase.screenplay.ui;

import org.openqa.selenium.By;

import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.screenplay.targets.Target;

public class HospitalDoctorHomePage extends PageObject {
	
	public static final Target BOTON__AGREGAR_DOCTOR= Target.the("Botón Agregar Doctor")
			 .located(By.linkText("Agregar Doctor"));	
	public static final Target NOMBRE_DOCTOR= Target.the("Nombre Doctor")
			 .located(By.id("name"));
	public static final Target APELLIDO_DOCTOR= Target.the("Apellido Doctor ")
			 .located(By.id("last_name"));
	public static final Target TELEFONO_DOCTOR= Target.the("Telefono Doctor")
			 			 .located(By.id("telephone"));
	public static final Target DOCUMENTO_DOCTOR= Target.the("TDocumento Doctor")
			 			 .located(By.id("identification_type"));
	public static final Target NUMERO_DOCUMENTO_DOCTOR= Target.the("NumeroDocumento Doctor")
			 .located(By.id("identification"));
	public static final Target BOTON_GUARDAR_DOCTOR= Target.the("Guardar Doctor")
			 .located(By.xpath("//A[@onclick='submitForm()'][text()='Guardar']"));
	

}


