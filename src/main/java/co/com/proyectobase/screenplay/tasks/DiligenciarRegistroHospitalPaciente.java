package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.ui.HospitalDoctorHomePage;
import co.com.proyectobase.screenplay.ui.HospitalPacienteHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class DiligenciarRegistroHospitalPaciente implements Task {
	
	private List<List<String>> data;

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(HospitalPacienteHomePage.BOTON_AGREGAR_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(0).trim()).into(HospitalPacienteHomePage.NOMBRE_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(1).trim()).into(HospitalPacienteHomePage.APELLIDO_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(2).trim()).into(HospitalPacienteHomePage.TELEFONO_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(3).trim()).into(HospitalPacienteHomePage.NUMERO_DOCUMENTO_PACIENTE));
		actor.attemptsTo(Click.on(HospitalPacienteHomePage.SALUD_PREPAGADA));
		actor.attemptsTo(Click.on(HospitalPacienteHomePage.BOTON_GUARDAR_PACIENTE));
		
	}
	public static DiligenciarRegistroHospitalPaciente RegistroPaciente(List<List<String>> data) {
		return Tasks.instrumented(DiligenciarRegistroHospitalPaciente.class, data);
		
	}
	public DiligenciarRegistroHospitalPaciente(List<List<String>> data) {
		this.data = data;
}
}
