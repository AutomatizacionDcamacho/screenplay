package co.com.proyectobase.screenplay.tasks;



public class DiligenciarRegistroOrange  {

			private String pim;
			private String primernombre;
			private String apellido;
			private String id;
			private String ubicacion;
			private String segundonombre;
			private String id2;
			private String cumpleaños;
			private String estadocivil;
			private String nacionalidad;
			private String licencia;
			private String venLicencia;
			private String nickname;
			private String tiposangre;
			private String hobbie;
			private String busqueda;
			public String getPim() {
				return pim;
			}
			public void setPim(String pim) {
				this.pim = pim;
			}
			public String getPrimernombre() {
				return primernombre;
			}
			public void setPrimernombre(String primernombre) {
				this.primernombre = primernombre;
			}
			public String getApellido() {
				return apellido;
			}
			public void setApellido(String apellido) {
				this.apellido = apellido;
			}
			public String getId() {
				return id;
			}
			public void setId(String id) {
				this.id = id;
			}
			public String getUbicacion() {
				return ubicacion;
			}
			public void setUbicacion(String ubicacion) {
				this.ubicacion = ubicacion;
			}
			public String getSegundonombre() {
				return segundonombre;
			}
			public void setSegundonombre(String segundonombre) {
				this.segundonombre = segundonombre;
			}
			public String getId2() {
				return id2;
			}
			public void setId2(String id2) {
				this.id2 = id2;
			}
			public String getCumpleaños() {
				return cumpleaños;
			}
			public void setCumpleaños(String cumpleaños) {
				this.cumpleaños = cumpleaños;
			}
			public String getEstadocivil() {
				return estadocivil;
			}
			public void setEstadocivil(String estadocivil) {
				this.estadocivil = estadocivil;
			}
			public String getNacionalidad() {
				return nacionalidad;
			}
			public void setNacionalidad(String nacionalidad) {
				this.nacionalidad = nacionalidad;
			}
			public String getLicencia() {
				return licencia;
			}
			public void setLicencia(String licencia) {
				this.licencia = licencia;
			}
			public String getVenLicencia() {
				return venLicencia;
			}
			public void setVenLicencia(String venLicencia) {
				this.venLicencia = venLicencia;
			}
			public String getNickname() {
				return nickname;
			}
			public void setNickname(String nickname) {
				this.nickname = nickname;
			}
			public String getTiposangre() {
				return tiposangre;
			}
			public void setTiposangre(String tiposangre) {
				this.tiposangre = tiposangre;
			}
			public String getHobbie() {
				return hobbie;
			}
			public void setHobbie(String hobbie) {
				this.hobbie = hobbie;
			}
			public String getBusqueda() {
				return busqueda;
			}
			public void setBusqueda(String busqueda) {
				this.busqueda = busqueda;
			}
		
	}


