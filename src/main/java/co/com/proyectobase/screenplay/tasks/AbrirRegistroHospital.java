package co.com.proyectobase.screenplay.tasks;


import co.com.proyectobase.screenplay.ui.HospitalHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirRegistroHospital implements Task {
	
private HospitalHomePage hospitalHomePage;
	

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(hospitalHomePage));
	
	
}

	public static Performable LapaginaRegitroHospitales() {
		return Tasks.instrumented(AbrirRegistroHospital.class);
	}
	
}