package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import co.com.proyectobase.screenplay.ui.HospitalAgendarCitaHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class DiligenciarRegistroHospitalAgendarCita implements Task {
	
	private List<List<String>> data;

	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(HospitalAgendarCitaHomePage.BOTON_AGENDAR_CITA));
		actor.attemptsTo(Enter.theValue(data.get(1).get(0).trim()).into(HospitalAgendarCitaHomePage.DIA_CITA));
		actor.attemptsTo(Click.on(HospitalAgendarCitaHomePage.CLICK));
		actor.attemptsTo(Enter.theValue(data.get(1).get(1).trim()).into(HospitalAgendarCitaHomePage.DOC_PACIENTE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(2).trim()).into(HospitalAgendarCitaHomePage.DOC_DOCTOR));
		actor.attemptsTo(Enter.theValue(data.get(1).get(3).trim()).into(HospitalAgendarCitaHomePage.OBSERVACIONES));
		actor.attemptsTo(Click.on(HospitalAgendarCitaHomePage.BOTON_GUARDAR_CITA));
		
	}
	public static DiligenciarRegistroHospitalAgendarCita RegistroCita(List<List<String>> data) {
		return Tasks.instrumented(DiligenciarRegistroHospitalAgendarCita.class, data);
		
	}
	public DiligenciarRegistroHospitalAgendarCita(List<List<String>> data) {
		this.data = data;
}
	
}