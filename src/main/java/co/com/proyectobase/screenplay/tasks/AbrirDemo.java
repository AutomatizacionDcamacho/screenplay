package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.DemoHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirDemo implements Task {
	
	private DemoHomePage demoHomePage;

	@Override
	public <T extends Actor> void performAs(T actor) {
			actor.attemptsTo(Open.browserOn(demoHomePage));
	}

	public static AbrirDemo LapaginaDemo() {
		return Tasks.instrumented(AbrirDemo.class);
	}

}
