package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.WebElement;

import co.com.proyectobase.screenplay.ui.DemoHomePage;
import co.com.proyectobase.screenplay.ui.OrageHomePage;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromTarget;

public class DiligenciarRegistro implements Task{
	

	private List<List<String>> data;
	


	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Click.on(OrageHomePage.BOTON_LOGIN));
		actor.attemptsTo(Click.on(OrageHomePage.BOTON_PMI));
		actor.attemptsTo(Click.on(OrageHomePage.BOTON_ADD));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		actor.attemptsTo(Enter.theValue(data.get(1).get(1).trim()).into(OrageHomePage.AREA_PRIMERNOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(2).trim()).into(OrageHomePage.AREA_APELLIDO));
		actor.attemptsTo(Enter.theValue(data.get(1).get(3).trim()).into(OrageHomePage.AREA_ID1));
		actor.attemptsTo(Click.on(OrageHomePage.LISTA_UBICACION));
		clic_objeto_lista_ubicacion(OrageHomePage.UBICACION.resolveFor(actor), data.get(1).get(4).trim());
		actor.attemptsTo(Click.on(OrageHomePage.BOTON_GUARDAR));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		actor.attemptsTo(Enter.theValue(data.get(1).get(5).trim()).into(OrageHomePage.AREA_SEGUNDO_NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(6).trim()).into(OrageHomePage.AREA_ID)); 
		actor.attemptsTo(Enter.theValue(data.get(1).get(7).trim()).into(OrageHomePage.FECHA_CUMPLE));
		actor.attemptsTo(Click.on(OrageHomePage.ESTADO_CIVIL));
		clic_objeto_lista(OrageHomePage.LISTA_ESTADO_CIVIL.resolveFor(actor), data.get(1).get(8).trim());
		actor.attemptsTo(Click.on(OrageHomePage.GENERO));
		actor.attemptsTo(Click.on(OrageHomePage.NACIONALIDAD));
		clic_objeto_lista(OrageHomePage.LISTA_NACIONALIDAD.resolveFor(actor), data.get(1).get(9).trim());
		actor.attemptsTo(Enter.theValue(data.get(1).get(10).trim()).into(OrageHomePage.AREA_LICENCIA));
		actor.attemptsTo(Enter.theValue(data.get(1).get(11).trim()).into(OrageHomePage.AREA_VENCIMIENTO_LICENCIA));
		actor.attemptsTo(Enter.theValue(data.get(1).get(12).trim()).into(OrageHomePage.AREA_NICKNAME));
		actor.attemptsTo(Click.on(OrageHomePage.AREA_TIPO_SANGRE));
		clic_objeto_lista(OrageHomePage.LISTA_TIPO_SANGRE.resolveFor(actor), data.get(1).get(13).trim());
		actor.attemptsTo(Enter.theValue(data.get(1).get(14).trim()).into(OrageHomePage.AREA_HOBBIE));
		actor.attemptsTo(Click.on(OrageHomePage.BOTON_SALVAR ));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		actor.attemptsTo(Click.on(OrageHomePage.OPCION_CONSULTAR));
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		} 
		actor.attemptsTo(Enter.theValue(data.get(1).get(15).trim()).into(OrageHomePage.CAMPO_BUSQUEDA));
		actor.attemptsTo(Click.on(OrageHomePage.BOTON_BUSQUEDA ));
		/*actor.attemptsTo(Enter.theValue(data.get(1).get(16).trim()).into(OrageHomePage.CAMPO_VERIFICAR));*/
	try {
		Thread.sleep(10000);
	} catch (InterruptedException e) {
		e.printStackTrace();
		
		
		
	}
	}
	public void clic_objeto_lista_ubicacion(WebElementFacade objeto, String strDato) {
		List<WebElement> listElementos = objeto.findElements(By.tagName("ul"));
		for (WebElement e : listElementos) {
			if (e.getText().contains(strDato)) {
				e.click();
				break;
			}
		}
	}

	public void clic_objeto_lista(WebElementFacade objeto, String strDato) {
		
		List<WebElement> listElementos = objeto.findElements(By.tagName("li"));
		for (WebElement e : listElementos) {
			if (e.getText().equals(strDato)) {
				e.click();
				break;
			}
		}
	}


	public static DiligenciarRegistro Formulario(List<DiligenciarRegistroOrange> diligenciarRegistroOrange) {
		return Tasks.instrumented(DiligenciarRegistro.class, diligenciarRegistroOrange);
	}
	
	public DiligenciarRegistro(List<List<String>> data) {
	this.data = data;
}
}



