package co.com.proyectobase.screenplay.tasks;

import java.util.List;

import org.openqa.selenium.WebElement;

import co.com.proyectobase.screenplay.ui.DemoHomePage;
import co.com.proyectobase.screenplay.ui.GoogleTraductorPage;
import net.serenitybdd.core.annotations.findby.By;
import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Performable;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.serenitybdd.screenplay.actions.SelectFromOptions;
import net.serenitybdd.screenplay.actions.selectactions.SelectByVisibleTextFromTarget;
import net.serenitybdd.screenplay.targets.Target;

public class Diligenciar implements Task {
	
	private List<List<String>> data;
	
	
	@Override
	public <T extends Actor> void performAs(T actor) {
		
		actor.attemptsTo(Enter.theValue(data.get(1).get(0).trim()).into(DemoHomePage.AREA__NOMBRE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(1).trim()).into(DemoHomePage.AREA_APELLIDO));
		actor.attemptsTo(Enter.theValue(data.get(1).get(2).trim()).into(DemoHomePage.AREA_DIRECCION));
		actor.attemptsTo(Enter.theValue(data.get(1).get(3).trim()).into(DemoHomePage.AREA_EMAIL));
		actor.attemptsTo(Enter.theValue(data.get(1).get(4).trim()).into(DemoHomePage.AREA_TELEFONO));
		actor.attemptsTo(Click.on(DemoHomePage.AREA_GENERO));
		actor.attemptsTo(Click.on(DemoHomePage.AREA_HOBBIE));
		actor.attemptsTo(Click.on(DemoHomePage.AREA_LENGUAJE));
		clic_objeto_lista(DemoHomePage.LISTA_LENGUAJE.resolveFor(actor), data.get(1).get(5).trim());
		actor.attemptsTo(new SelectByVisibleTextFromTarget(DemoHomePage.AREA_SKILL, data.get(1).get(6).trim()));
		actor.attemptsTo(new SelectByVisibleTextFromTarget(DemoHomePage.AREA_PAIS,data.get(1).get(7).trim()));
		actor.attemptsTo(Click.on(DemoHomePage.AREA_CIUDAD));
		clic_objeto_lista(DemoHomePage.LISTA_CIUDAD.resolveFor(actor),  data.get(1).get(8).trim());
		actor.attemptsTo(new SelectByVisibleTextFromTarget(DemoHomePage.AREA_CUMPLE,data.get(1).get(9).trim()));
		actor.attemptsTo(new SelectByVisibleTextFromTarget(DemoHomePage.LISTA_MesCUMPLE,data.get(1).get(10).trim()));
		actor.attemptsTo(new SelectByVisibleTextFromTarget(DemoHomePage.AREA_DIA_CUMPLE,data.get(1).get(11).trim()));
		actor.attemptsTo(Enter.theValue(data.get(1).get(12).trim()).into(DemoHomePage.AREA_CLAVE));
		actor.attemptsTo(Enter.theValue(data.get(1).get(13).trim()).into(DemoHomePage.AREA_CONFIRMARCLAVE));
		actor.attemptsTo(Click.on(DemoHomePage.BOTON_ENVIAR));
				
	}			
		

	public void clic_objeto_lista(WebElementFacade objeto, String strDato) {
		
		List<WebElement> listElementos = objeto.findElements(By.tagName("li"));
		for (WebElement e : listElementos) {
			if (e.getText().equals(strDato)) {
				e.click();
				break;
			}
		}
	}
	

		

public Diligenciar(List<List<String>> data) {
		this.data = data;
	}



public static Diligenciar elFormulario(List<List<String>> data) {
	
	return Tasks.instrumented(Diligenciar.class, data) ;
}
}