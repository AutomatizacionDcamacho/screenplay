package co.com.proyectobase.screenplay.tasks;


import java.util.List;

import co.com.proyectobase.screenplay.ui.HospitalDoctorHomePage;
import co.com.proyectobase.screenplay.ui.OrageHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;

public class DiligenciarResgitroHospitalDoctor implements Task {
	
	private List<List<String>> data;




	@Override
		public <T extends Actor> void performAs(T actor) {
				
			actor.attemptsTo(Click.on(HospitalDoctorHomePage.BOTON__AGREGAR_DOCTOR));
			actor.attemptsTo(Enter.theValue(data.get(1).get(0).trim()).into(HospitalDoctorHomePage.NOMBRE_DOCTOR));
			actor.attemptsTo(Enter.theValue(data.get(1).get(1).trim()).into(HospitalDoctorHomePage.APELLIDO_DOCTOR));
			actor.attemptsTo(Enter.theValue(data.get(1).get(2).trim()).into(HospitalDoctorHomePage.TELEFONO_DOCTOR));
			actor.attemptsTo(Enter.theValue(data.get(1).get(3).trim()).into(HospitalDoctorHomePage.NUMERO_DOCUMENTO_DOCTOR));
			actor.attemptsTo(Click.on(HospitalDoctorHomePage.BOTON_GUARDAR_DOCTOR));
				
	}
	public static DiligenciarResgitroHospitalDoctor RegistroDoctor(List<List<String>> data) {
		return Tasks.instrumented(DiligenciarResgitroHospitalDoctor.class, data);
		
	}
	public DiligenciarResgitroHospitalDoctor(List<List<String>> data) {
	this.data = data;
}
	
}
	
	


