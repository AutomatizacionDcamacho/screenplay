package co.com.proyectobase.screenplay.tasks;

import co.com.proyectobase.screenplay.ui.OrageHomePage;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.Tasks;
import net.serenitybdd.screenplay.actions.Open;

public class AbrirOrage implements Task{
	
	private OrageHomePage orageHomePage;

	@Override
	public <T extends Actor> void performAs(T actor) {
		actor.attemptsTo(Open.browserOn(orageHomePage));

		
	}

	public static AbrirOrage LaPaginaOrage() {
		return Tasks.instrumented(AbrirOrage.class);
	}

}
