#Author: dccamacho@choucairtesting.com
# language:es

@Regresion
Característica:   	Como paciente
Quiero realizar la solicitud de una cita médica
	A través del sistema de Administración de Hospitales


 @Escenario1
   Escenario: Realizar el Registro de un Doctor 
   Dado que Carlos necesita registrar un nuevo doctor
   Cuando el realiza el registro del mismo en el aplicativo de Administración de Hospitales
   |Nombre       |Apellido|Telefono|NroDocumentoDoctor|
   |Andres Felipe|Diaz|3014951241|3243633|
   Entonces el verifica que se presente en pantalla el mensaje Datos guardados correctamente ("Datos guardados correctamente.")

@Escenario2
	Escenario: Realizar el Registro de un Paciente
	Dado que Carlos necesita registrar un nuevo paciente
	Cuando el realiza el registro del paciente en el aplicativo de Administración de Hospitales
	|Nombre|Apellido|Telefono|NroDocumentoPaciente|
  |Carolina|Lopez|3014951241|73983237324|
	Entonces el verifica que se presente en pantalla el mensaje Datos del paciente guardados correctamente ("Datos guardados correctamente.")
	
@Escenario3
	Escenario: Realizar el Agendamiento de una Cita
	Dado que Carlos necesita asistir al medico
	Cuando el realiza el agendamiento de una Cita
	|FechaCita	|NroDocumentoPaciente|NroDocumentoDoctor|Observacion|
  |30/09/2018|73983237324|3243633|Cita médica|
	Entonces el verifica que se presente en pantalla el mensaje de Datos de la cita guardados correctamente	("Datos guardados correctamente.")
	
	