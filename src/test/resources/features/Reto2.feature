#Author: dccamacho@choucairtesting.com
# language:es

@tag
Característica: Ingresar a la pagina OrageHR
  Realizar el registro a la aplicación 
  Para visualizar el empleado creado

  @CasoExitoso
   Escenario: Crear y consultar el empleado en la pagina 
   Dado que Juan necesita crear un empleado en el OrageHR
   Cuando el realiza el ingreso del registro en la aplicación
   |pim         |primer nombre|apellido|id   |ubicacion             |segundo nombre|id2    |cumpleaños|estadocivil|nacionalidad| licencia  |venLicencia|nickname|tiposangre|hobbie|busqueda|
   |Add Employee|Diana        |Camacho |17785|Australian Regional HQ|Carolina      |1786  |1985/03/26| Soltero/a |  American  |85032638738|2020/03/26 |dcamacho|O         |Leer  |17785   |
   Entonces el visualiza el nuevo empleado en el aplicativo con ID 17729


