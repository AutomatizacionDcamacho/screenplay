package co.com.proyectobase.screenplay.stepdefinitions;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;

import org.openqa.selenium.WebDriver;


import co.com.proyectobase.screenplay.questions.LaValidacionDatosRegistroHospital;
import co.com.proyectobase.screenplay.tasks.AbrirRegistroHospital;
import co.com.proyectobase.screenplay.tasks.DiligenciarRegistroHospitalPaciente;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Reto3Scenario2StepDefinition {
	

	@Managed(driver= "chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");
	
	@Before
	public void configuracionInicial()
	{
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
		
		@Dado("^que Carlos necesita registrar un nuevo paciente$")
		public void queCarlosNecesitaRegistrarUnNuevoPaciente()  {
			carlos.wasAbleTo(AbrirRegistroHospital.LapaginaRegitroHospitales());
	    
		}
	    
		@Cuando("^el realiza el registro del paciente en el aplicativo de Administración de Hospitales$")
		public void elRealizaElRegistroDelMismoEnElAplicativoDeAdministraciónDeHospitales(DataTable dtDataFormulario)  {
	    	List<List<String>> data= dtDataFormulario.raw();
	    	carlos.attemptsTo(DiligenciarRegistroHospitalPaciente.RegistroPaciente(data));
		   
	    }
		@Entonces("^el verifica que se presente en pantalla el mensaje Datos del paciente guardados correctamente \\(\"([^\"]*)\"\\)$")
		public void elVerificaQueSePresenteEnPantallaElMensajeDatosDelPacienteGuardadosCorrectamente(String label) throws Exception {
			carlos.should(seeThat(LaValidacionDatosRegistroHospital.es(), equalTo(label)));
			System.out.print(label);
			}

		}
	




