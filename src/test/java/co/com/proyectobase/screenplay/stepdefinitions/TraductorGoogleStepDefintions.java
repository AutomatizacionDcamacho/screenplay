package co.com.proyectobase.screenplay.stepdefinitions;

import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaRespuesta;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.Traducir;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class TraductorGoogleStepDefintions {
	
	private static final String String = null;
	@Managed(driver="chrome")
	private WebDriver hisBrowser;
	private Actor rafa= Actor.named("Rafa");
	
	@Before
	public void configuracionInicial()
	{
		rafa.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Given("^Que Rafa quiere usar el traductor de google$")
	public void queRafaQuiereUsarElTraductorDeGoogle()  {
	  rafa.wasAbleTo(Abrir.LaPaginaDeGoogle());
	}
	@When("^el traduce la palabra (.*) de Ingles a Español$")
	  public void elTraduceLaPalabraDeInglesAEspañolLa(String palabra) {
		rafa.attemptsTo(Traducir.DeInglesAEspanol(palabra));
	}

	@Then("^El deberia ver la palabra (.*) en la pantalla$")
	public void elDeberiaVerLaPalabraMesaEnLaPantalla(String palabraEsperada)  {
		rafa.should (seeThat(LaRespuesta.es(), equalTo(palabraEsperada)));
	}
}	


