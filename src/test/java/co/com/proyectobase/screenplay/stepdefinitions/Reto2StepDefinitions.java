package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;


import org.hamcrest.Matchers;
import org.openqa.selenium.WebDriver;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import co.com.proyectobase.screenplay.questions.LaComparacion;
import co.com.proyectobase.screenplay.tasks.AbrirOrage;
import co.com.proyectobase.screenplay.tasks.DiligenciarRegistro;
import co.com.proyectobase.screenplay.tasks.DiligenciarRegistroOrange;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;


public class Reto2StepDefinitions {
	
	@Managed(driver= "chrome")
	private WebDriver hisBrowser;
	private Actor juan = Actor.named("Juan");
	
	@Before
	public void configuracionInicial()
	{
		juan.can(BrowseTheWeb.with(hisBrowser));
	}
	
	
	
	@Dado("^que Juan necesita crear un empleado en el OrageHR$")
	public void queJuanNecesitaCrearUnEmpleadoEnElOrageHR()  {
		juan.wasAbleTo(AbrirOrage.LaPaginaOrage());
	}

	@Cuando("^el realiza el ingreso del registro en la aplicación$")
	public void elRealizaElIngresoDelRegistroEnLaAplicación(List<DiligenciarRegistroOrange>diligenciarRegistroOrange) {
	   juan.attemptsTo(DiligenciarRegistro.Formulario(diligenciarRegistroOrange));
	}

		@Entonces("^el visualiza el nuevo empleado en el aplicativo con ID (.*)$")
		public void elVisualizaElNuevoEmpleadoEnElAplicativoConID(String label) throws Exception {
			
	}




}
