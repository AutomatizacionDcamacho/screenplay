package co.com.proyectobase.screenplay.stepdefinitions;

import java.util.List;

import org.hamcrest.Matcher;
import static org.hamcrest.Matchers.equalTo;
import org.openqa.selenium.WebDriver;

import co.com.proyectobase.screenplay.questions.LaValidacion;
import co.com.proyectobase.screenplay.tasks.Abrir;
import co.com.proyectobase.screenplay.tasks.AbrirDemo;
import co.com.proyectobase.screenplay.tasks.Diligenciar;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Reto1StepDefinitions {

	@Managed (driver = "chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");	
	
	@Before
	public void configuracionInicial()
	{
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}
	
	@Dado("^que Carlos quiere acceder a la Web Automation Demo Site$")
	public void queCarlosQuiereAccederALaWebAutomationDemoSite()  {
	    carlos.wasAbleTo(AbrirDemo.LapaginaDemo());
	}

	@Cuando("^el realiza el registro en la página$")
	public void el_realiza_el_registro_en_la_página(DataTable dtDataFormulario)  {
		List<List<String>> data= dtDataFormulario.raw();
		carlos.attemptsTo(Diligenciar.elFormulario(data));
	}
		
	@Entonces("^el verifica que se carga la pantalla con texto (.*)")
	public void elVerificaQueSeCargaLaPantallaConTextoDoubleClickOnEditIconToEDITTheTableRow(String label)  {
	    carlos.should(seeThat(LaValidacion.es(),equalTo("- Double Click on Edit Icon to EDIT the Table Row.")));
	}



}
