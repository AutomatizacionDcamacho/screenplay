package co.com.proyectobase.screenplay.stepdefinitions;

import org.openqa.selenium.WebDriver;

import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.List;


import co.com.proyectobase.screenplay.questions.LaValidacionDatosRegistroHospital;
import co.com.proyectobase.screenplay.tasks.AbrirRegistroHospital;
import co.com.proyectobase.screenplay.tasks.DiligenciarResgitroHospitalDoctor;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;
import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import net.thucydides.core.annotations.Managed;

public class Reto3Scenario1StepDefinition {

	@Managed(driver = "chrome")
	private WebDriver hisBrowser;
	private Actor carlos = Actor.named("Carlos");

	@Before
	public void configuracionInicial() {
		carlos.can(BrowseTheWeb.with(hisBrowser));
	}

	@Dado("^que Carlos necesita registrar un nuevo doctor$")
	public void queCarlosNecesitaRegistrarUnNuevoDoctor() {
		carlos.wasAbleTo(AbrirRegistroHospital.LapaginaRegitroHospitales());
	}

	@Cuando("^el realiza el registro del mismo en el aplicativo de Administración de Hospitales$")
	public void elRealizaElRegistroDelMismoEnElAplicativoDeAdministraciónDeHospitales(DataTable dtDataFormulario) {
		List<List<String>> data = dtDataFormulario.raw();
		carlos.attemptsTo(DiligenciarResgitroHospitalDoctor.RegistroDoctor(data));
	}

	
	@Entonces("^el verifica que se presente en pantalla el mensaje Datos guardados correctamente \\(\"([^\"]*)\"\\)$")
	public void elVerificaQueSePresenteEnPantallaElMensajeDatosGuardadosCorrectamente(String label) throws Exception {
		carlos.should(seeThat(LaValidacionDatosRegistroHospital.es(), equalTo(label)));
		System.out.print(label);
	}

}
